package com.example.iqsikinoliki1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.*
import kotlinx.android.synthetic.main.activity_main.*
private var firstPlayer = true

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }
    private fun init() {
        button00.setOnClickListener{
            checkPlayer(button00)
            restart()
        }
        button01.setOnClickListener{
            checkPlayer(button01)
            restart()
        }
        button02.setOnClickListener{
            checkPlayer(button02)
            restart()
        }
        button03.setOnClickListener{
            checkPlayer(button03)
            restart()
        }
        button04.setOnClickListener{
            restart()
            checkPlayer(button04)
        }
        button05.setOnClickListener{
            restart()
            checkPlayer(button05)
        }
        button06.setOnClickListener{
            restart()
            checkPlayer(button06)
        }
        button07.setOnClickListener{
            restart()
            checkPlayer(button07)
        }
        button08.setOnClickListener{
            restart()
            checkPlayer(button08)
        }
    }
    private fun checkPlayer(button: Button){
        if (button.text.isEmpty()){
            if (firstPlayer){
                button.text = "X"
                firstPlayer = false
            }else{
                button.text = "0"
                firstPlayer = true
            }
            if (button00.text.isNotEmpty() && button00.text == button01.text && button00.text == button02.text){
                Toast.makeText(this,"winner is ${button00.text}",Toast.LENGTH_LONG).show()
                ifWinner()
                restart()
            }else if (button03.text.isNotEmpty() && button03.text == button04.text && button03.text == button05.text){
                Toast.makeText(this,"winner is ${button03.text}",Toast.LENGTH_LONG).show()
                ifWinner()
                restart()
            }else if (button06.text.isNotEmpty() && button06.text == button07.text && button06.text == button08.text){
                Toast.makeText(this,"winner is ${button06.text}",Toast.LENGTH_LONG).show()
                ifWinner()
                restart()
            }else if (button00.text.isNotEmpty() && button00.text == button03.text && button00.text == button06.text){
                Toast.makeText(this,"winner is ${button00.text}",Toast.LENGTH_LONG).show()
                ifWinner()
                restart()
            }else if (button01.text.isNotEmpty() && button01.text == button04.text && button01.text == button07.text){
                Toast.makeText(this,"winner is ${button01.text}",Toast.LENGTH_LONG).show()
                ifWinner()
                restart()
            }else if (button02.text.isNotEmpty() && button02.text == button05.text && button02.text == button08.text){
                Toast.makeText(this,"winner is ${button02.text}",Toast.LENGTH_LONG).show()
                ifWinner()
                restart()
            }else if (button00.text.isNotEmpty() && button00.text == button04.text && button00.text == button08.text){
                Toast.makeText(this,"winner is ${button00.text}",Toast.LENGTH_LONG).show()
                ifWinner()
                restart()
            }else if (button02.text.isNotEmpty() && button02.text == button04.text && button02.text == button06.text){
                Toast.makeText(this,"winner is ${button02.text}",Toast.LENGTH_LONG).show()
                ifWinner()
                restart()
            }else if (button00.text.isNotEmpty() && button01.text.isNotEmpty() && button02.text.isNotEmpty() && button03.text.isNotEmpty() && button04.text.isNotEmpty() && button05.text.isNotEmpty() && button06.text.isNotEmpty() && button07.text.isNotEmpty() && button08.text.isNotEmpty()){
                Toast.makeText(this,"ფრეა და დააჭირე restart-ს",Toast.LENGTH_LONG).show()
            }
        }
    }private fun ifWinner(){
        button00.isClickable = false
        button01.isClickable = false
        button02.isClickable = false
        button03.isClickable = false
        button04.isClickable = false
        button05.isClickable = false
        button06.isClickable = false
        button07.isClickable = false
        button08.isClickable = false
    }private  fun restart(){
        button10.setOnClickListener(){
            recreate()
        }

    }
}